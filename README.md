
## Run Spring Boot application
run this code in your terminal.
```
mvn spring-boot:run
```
Please Check the Postman json file for the API test.
BaseURL : **localhost:8080/api** (the development is on localhost, depends on your server envoirment)
| No | Method | Endpoint | Body|
|--|--|--|--|
|1|POST  |/orders  |create order data (for testing purpose)|
|2| POST|/pending/returns  |create token  |
|3| POST |/returns|create return by order item  |
|4| GET |/returns/:id|get return data|
|5| PUT |/returns/:returnId/items/:returnItemId/qc/status|update qc status  |
|6| PUT |/returns/4|update return status  |


## API Request Body

all the api request and response uses **json**,
1. POST- localhost:8080/api/orders
>     { 
>         "orderId" : "RK-14256",
>         "emailAddress" : "dammitjoe@gmail.com" ,
>         "sku" : "ASFF21-123" ,
>         "quantity" : 5 ,
>         "price" : 2.42 ,
>         "itemName" : "Necktie" 
>     }
2. POST- localhost:8080/api/pending/returns
>     {
>          "orderId" : "RK-14256",
>          "emailAddress" : "dammitjoe@gmail.com"
>     }
3. POST- localhost:8080/api/returns
Header-Authorization : Bearer Token (Required)
>     {
>           "orderId" : "124214214",
>           "emailAddress" : "dammitjoe" ,
>            "returnItems": [{
>             "amount" : 4,
>             "sku": "ASFF21-123"
>           }]
>     }

4. GET - localhost:8080/api/returns/:id

    enter code here

>      "data": {
>              "refund_amount": 9.68,
>              "return": {
>                       "id": 4,
>                      "orderId": "RK-14256",
>                      "emailAddress": "dammitjoe@gmail.com",
>                      "status": ""
>                },
>               "items": [{
>                        "id": 5,
>                       "returnId": 4,
>                       "sku": "ASFF21-123",
>                       "amount": 4,
>                       "qcStatus": "ACCEPTED"
>                }]
>              

5. PUT- localhost:8080/api/returns/:returnId/items/:itemId/qc/status
>     {
>          "qcStatus": "ACCEPTED" //  Value is ACCEPTED or REJECTED
>     }

6. PUT- localhost:8080/api/returns/4
>     {
>         "status": "COMPLETE" // Value is COMPLETE or AWAITING_APPROVAL
>     }