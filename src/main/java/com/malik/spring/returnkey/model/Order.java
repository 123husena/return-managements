/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.malik.spring.returnkey.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "orders")
public class Order implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

        @Column(name = "emailAddress")
	private String emailAddress;
        
	@Column(name = "orderId")
	private String orderId;

	@Column(name = "sku",unique=true)
	private String sku;

	@Column(name = "quantity")
	private int quantity;
        
	@Column(name = "price")
	private double price;

	@Column(name = "itemName")
	private String itemName;
        
    public Order(){}

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

	public Order(String emailAddress, String orderId, String sku, int quantity, double price, String itemName) {
		this.emailAddress = emailAddress;
		this.orderId = orderId;
		this.sku = sku;
		this.quantity = quantity;
		this.price = price;
		this.itemName = itemName;
	}
	public long getId() {
		return id;
	}
}
