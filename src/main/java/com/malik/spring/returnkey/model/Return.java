package com.malik.spring.returnkey.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author C030122001
 */


@Entity
@Table(name = "returns")
public class Return {
        public Return() {}
    
    	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name = "orderId")
	private String orderId;
        
	@Column(name = "emailAddress")
	private String emailAddress;        

    @NotNull
	@Column(name = "is_partial")
	private boolean isPartial;
        
    
 	@Column(name = "status", nullable = true)
	private String status = "";   
        
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

	public Return(String emailAddress, String orderId,String status) {
		this.emailAddress = emailAddress;
		this.orderId = orderId;
		this.status = status;
	}
        
    
}
