package com.malik.spring.returnkey.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author C030122001
 */

@Entity
@Table(name = "order_return_tokens")
public class OrderReturnToken {
    public OrderReturnToken(){}
    
    	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

        @Column(name = "emailAddress")
	private String emailAddress;

	@Column(name = "orderId")
	private String orderId;

	@Column(name = "token")
	private String token;

	@Column(name = "status")
	private String status;        

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    public OrderReturnToken(String emailAddress, String orderId, String token, String status) {
            this.emailAddress = emailAddress;
            this.orderId = orderId;
            this.token = token;
            this.status = status;                
    }
}
