package com.malik.spring.returnkey.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;

public class ReturnRequest {
    @SerializedName("orderId")
    @Expose
    private String orderId;
    @SerializedName("emailAddress")
    @Expose
    private String emailAddress;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("returnItems")
    @Expose
    private List<ReturnItem> returnItems = null;

    public String getOrderId() {
    return orderId;
    }

    public void setOrderId(String orderId) {
    this.orderId = orderId;
    }

    public String getEmailAddress() {
    return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
    this.emailAddress = emailAddress;
    }

    public List<ReturnItem> getReturnItems() {
    return returnItems;
    }

    public void setReturnItems(List<ReturnItem> returnItems) {
    this.returnItems = returnItems;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
