package com.malik.spring.returnkey.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "return_items")
public class ReturnItem {
    	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name = "returnId")
	private long returnId;
        
	@Column(name = "sku")
	private String sku;       
        
	@Column(name = "amount")
	private int amount;       
        
 	@Column(name = "qcStatus", nullable = true)
	private String qcStatus = "";   
        

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }


    public long getReturnId() {
        return returnId;
    }

    public void setReturnId(long returnId) {
        this.returnId = returnId;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getQcStatus() {
        return qcStatus;
    }

    public void setQcStatus(String qcStatus) {
        this.qcStatus = qcStatus;
    }


    public ReturnItem() {}

    public ReturnItem(long returnId, int amount,  String sku,String qcStatus) {
        this.returnId = returnId;
        this.amount = amount;
        this.sku = sku;
        this.qcStatus = qcStatus;
    }
}
