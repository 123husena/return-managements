package com.malik.spring.returnkey.repository;

import com.malik.spring.returnkey.model.Return;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


public interface ReturnRepository extends JpaRepository<Return, Long> {
      List<Return> findByEmailAddressAndOrderId(String email,String orderId);
      
        @Modifying(clearAutomatically = true)
        @Query("update Return rtrn set rtrn.status =:status where rtrn.id =:id")
        int setStatusForReturn(@Param("id") Long id, @Param("status") String status);      
      
}
