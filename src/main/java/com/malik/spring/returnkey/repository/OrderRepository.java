package com.malik.spring.returnkey.repository;

import com.malik.spring.returnkey.model.Order;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order, Long> {
  List<Order> findByEmailAddressAndOrderId(String email,String orderId);
  
  List<Order> findByEmailAddressAndOrderIdAndSku(String email,String orderId,String sku);  
  
}
