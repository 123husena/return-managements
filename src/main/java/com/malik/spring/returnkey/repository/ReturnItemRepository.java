package com.malik.spring.returnkey.repository;

import com.malik.spring.returnkey.model.ReturnItem;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ReturnItemRepository extends JpaRepository<ReturnItem, Long> {
         List<ReturnItem> findByReturnId(long returnId);  
         List<ReturnItem> findBySku(String sku);  
      
        @Modifying(clearAutomatically = true)
        @Query("update ReturnItem rtrn set rtrn.qcStatus =:status where rtrn.id =:id and rtrn.returnId =:returnId")
        int setStatusForReturnItem(@Param("id") Long id, @Param("returnId") Long returnId, @Param("status") String status);        
      
}
