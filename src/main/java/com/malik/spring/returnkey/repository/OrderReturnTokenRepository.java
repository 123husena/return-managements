package com.malik.spring.returnkey.repository;

import com.malik.spring.returnkey.model.OrderReturnToken;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderReturnTokenRepository extends JpaRepository<OrderReturnToken, Long> {
  List<OrderReturnToken> findByToken(String token);
}

