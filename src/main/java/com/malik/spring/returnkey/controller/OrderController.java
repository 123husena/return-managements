
package com.malik.spring.returnkey.controller;

import com.malik.spring.returnkey.model.Order;
import com.malik.spring.returnkey.model.OrderReturnToken;
import com.malik.spring.returnkey.model.Return;
import com.malik.spring.returnkey.model.ReturnItem;
import com.malik.spring.returnkey.model.ReturnRequest;
import com.malik.spring.returnkey.repository.OrderRepository;
import com.malik.spring.returnkey.repository.OrderReturnTokenRepository;
import com.malik.spring.returnkey.repository.ReturnItemRepository;
import com.malik.spring.returnkey.repository.ReturnRepository;
import com.malik.spring.returnkey.utils.BearerTokenWrapper;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:8081")
@RestController
@RequestMapping("/api")
public class OrderController {
        private BearerTokenWrapper tokenWrapper;
	@Autowired
	OrderRepository orderRepository;
	@Autowired
	ReturnRepository returnRepository;
 	@Autowired
	ReturnItemRepository returnItemRepository;       
	@Autowired
	OrderReturnTokenRepository orderReturnTokenRepository;    
        
        public OrderController(BearerTokenWrapper tokenWrapper){
            this.tokenWrapper = tokenWrapper;
        }
        
	@GetMapping("/orders")
	public ResponseEntity<List<Order>> getAllOrders() {
		try {
			List<Order> orders = new ArrayList<Order>();
			orderRepository.findAll().forEach(orders::add);
                        if (orders.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<>(orders, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping("/orders")
	public ResponseEntity<Order> createOrder(@RequestBody Order order) {
		try {
			Order orders = orderRepository
					.save(new Order(order.getEmailAddress(), order.getOrderId(), order.getSku(), 
                                                order.getQuantity(), order.getPrice(), order.getItemName()));
			return new ResponseEntity<>(orders, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
        
	@PostMapping("/pending/returns")
	public Map<String,Object> createToken(@RequestBody Order order,HttpServletResponse response) {
            Map<String, Object> rtn = new LinkedHashMap<>();
		try {
                        List<Order> orders = new ArrayList<>();                    
			orderRepository.findByEmailAddressAndOrderId(order.getEmailAddress(),order.getOrderId()).forEach(orders::add);
			if (orders.isEmpty()) {
                                response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                                rtn.put("status", "Empty Data");
			}
                        String token = UUID.randomUUID().toString();
			orderReturnTokenRepository.saveAndFlush(new OrderReturnToken(order.getEmailAddress(), order.getOrderId(),token, "Active"));
                        rtn.put("token", token);
                        return rtn;
		} catch (Exception e) {
                    rtn.put("error", e);
		}
                return rtn;
	}
        
        @Transactional
	@PostMapping("/returns")
	public Map<String, Object> createReturns(@Valid @RequestBody ReturnRequest returnItem,HttpServletRequest request, HttpServletResponse response) {
            Map<String, Object> rtn = new LinkedHashMap<>();
            try {
                        List<OrderReturnToken> orderTokens = new ArrayList<>();
			orderReturnTokenRepository.findByToken(tokenWrapper.getToken()).forEach(orderTokens::add);
			if (orderTokens.isEmpty()) {
                                response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                                rtn.put("status", "Token Not found!");
                                return rtn;
			}
                        double totalAmount = 0;
                        OrderReturnToken orderToken = orderTokens.get(0);
                        Return returnData = returnRepository
					.saveAndFlush(new Return(
                                                orderToken.getEmailAddress(), 
                                                orderToken.getOrderId(), 
                                                returnItem.getStatus() != null ? returnItem.getStatus() : "" 
                                                ));      
                        for( ReturnItem rtrn : returnItem.getReturnItems()){
                            Order ordr = orderRepository.findByEmailAddressAndOrderIdAndSku(orderToken.getEmailAddress(), 
                                                orderToken.getOrderId(), rtrn.getSku()).get(0);                            
                            if(ordr == null){
                                 response.setStatus(HttpServletResponse.SC_NO_CONTENT);
                                 rtn.put("status", "Please provide the right SKU in every item!");
                                 return rtn;                               
                            }                   
                            List<ReturnItem> rtnItemList = returnItemRepository.findBySku(rtrn.getSku());  
                            if(rtnItemList.size() > 0){
                                 response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                                 rtn.put("status", "SKU Cannot be duplicate in this return transaction!!");
                                 return rtn;                                                      
                            }
                            returnItemRepository
                                            .saveAndFlush(new ReturnItem(
                                                    returnData.getId(), 
                                                    rtrn.getAmount() , 
                                                    rtrn.getSku() ,
                                                    ""
                                                    ));
                            

                           totalAmount += rtrn.getAmount() * ordr.getPrice();
                        }
                        rtn.put("refund_amount", totalAmount);
                        rtn.put("return", returnData);
                        return rtn;
		} catch (Exception e) {
                        TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                        rtn.put("error", e);
                        return rtn;
		}
	}
	@GetMapping("/returns/{id}")
	public Map<String,Object> getReturnById(@PathVariable long id,HttpServletResponse response) {
            Map<String, Object> rtn = new LinkedHashMap<>();
		try {
                        Return retData = returnRepository.findById(id).get();
                        if(retData == null){
                            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                            rtn.put("status", "Return Data Not Found!");
                            return rtn;                            
                        }
                        List<ReturnItem> returnItemList = returnItemRepository.findByReturnId(id);
                        double totalAmount = 0;
                        for(final ReturnItem retIt : returnItemList){
                            if(retIt.getQcStatus().equals("ACCEPTED")){
                                List<Order> orderData = orderRepository.findByEmailAddressAndOrderIdAndSku(retData.getEmailAddress(),retData.getOrderId(), retIt.getSku());
                                totalAmount = retIt.getAmount() * orderData.get(0).getPrice();
                            }
                        }
                        Map<String, Object> rtnRes = new LinkedHashMap<>();
                        rtnRes.put("refund_amount", totalAmount);
                        rtnRes.put("return",retData);
                        rtnRes.put("items", returnItemList);
                        rtn.put("data", rtnRes);
                        return rtn;
		} catch (Exception e) {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                        rtn.put("error", e);
                        return rtn;
                }
	}

        @Transactional
	@PutMapping("/returns/{id}/items/{itemId}/qc/status")
	public Map<String,Object> updateQcStatus(@RequestBody ReturnItem returnItem,@PathVariable long id,@PathVariable long itemId,HttpServletResponse response) {
            Map<String, Object> rtn = new LinkedHashMap<>();
		try {
                    
                        int updateReturn = returnItemRepository.setStatusForReturnItem(itemId,id,returnItem.getQcStatus());
                        if(updateReturn == 0){
                            rtn.put("status", "Fail to Update QC Status");
                            return rtn;
                        }
                        rtn.put("status", "Successfully Update QC Status");
                        return rtn;
		} catch (Exception e) {
                    rtn.put("error", e);
		}
                return rtn;
	}
                
        
        
        @Transactional
	@PutMapping("/returns/{id}")
	public Map<String,Object> updateReturnStatus(@RequestBody Return returnItem,@PathVariable long id,HttpServletResponse response) {
            Map<String, Object> rtn = new LinkedHashMap<>();
		try {
                        int updateReturn = returnRepository.setStatusForReturn(id,returnItem.getStatus());
                        if(updateReturn == 0){
                            rtn.put("status", "Fail to Update Return Data Status");
                            return rtn;
                        }
                        rtn.put("status", "Successfully Update Return Data");
                        return rtn;
		} catch (Exception e) {
                    rtn.put("error", e);
		}
                return rtn;
	}
        
}
